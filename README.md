# DTU DRIVEN templates

Document templates for DTU DRIVEN:
- Presentation slides (LaTeX, Keynote, PowerPoint)
- Posters (LaTeX).

# Funding statement

The Doctoral Training Unit 
**Data-driven computational modelling and applications** (DRIVEN)
is funded by the Luxembourg National Research Fund
under the PRIDE programme (PRIDE17/12252781).
